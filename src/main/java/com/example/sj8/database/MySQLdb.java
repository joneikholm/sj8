package com.example.sj8.database;

import com.example.sj8.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// AWS http://pushentertainment.com/rds-connections-by-instance-type/ shows about 52 simultaneous connections for Micro Instance
public class MySQLdb {
	//static final String DB_URL = "joneikholmdb.ciq8jfwpfby9.eu-central-1.rds.amazonaws.com/joneikholmdb";
	static final String DB_URL = "jdbc:mysql://joneikholmdb.ciq8jfwpfby9.eu-central-1.rds.amazonaws.com/joneikholmdb";
	static final String USER = "joneikholmdb";
	static final String PASS = "C323whBmNo06";
	private Connection conn = null;
	public MySQLdb(){
		try {
			createConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void createConnection() throws SQLException {
		System.out.println("Connecting to database..."); //STEP 1: Open a connection
		try {
			Class.forName("com.mysql.jdbc.Driver");

		conn = DriverManager.getConnection(DB_URL,USER,PASS);// automatically
		System.out.println(conn.getCatalog());//gets name of current database
		DatabaseMetaData metadata=conn.getMetaData(); // Here you can get info on the connection
		System.out.println(metadata.getUserName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<User> getAllUsers(){
		List<User> list = new ArrayList<>();
		Statement statement = null;
		try{
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE); //very cool: now we can update
			//direectly from any row in the resultset. Otherwise just create one without args.
			String sql = "SELECT * FROM users"; // select primary key, if need to update resultSet
			ResultSet resultSet = statement.executeQuery(sql); //STEP 3: execute query, assign result to a ResultSet object

			while(resultSet.next()) //STEP 4: Extract data from result set
			{
				String brugernavn = resultSet.getString("username");
				String password = resultSet.getString("password");
				int id = resultSet.getInt("idusers");
				User user = new User(brugernavn, password, id);
				list.add(user);
			}
			resultSet.close(); //STEP 5: Clean-up environment
			statement.close();
		}catch(SQLException se){
			System.out.println("Error connecting to MySQL Server");
			se.printStackTrace();
		}
		return list;
	}

	public void testQuery(){
		Statement statement = null;
		try{
			insert("anna", "1234");
			System.out.println("Creating statement..."); //STEP 2: Create Statement object
			statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE); //very cool: now we can update
			//direectly from any row in the resultset. Otherwise just create one without args.
			String sql = "SELECT * FROM users WHERE username ='anna' AND password='1234'"; // select primary key, if need to update resultSet
			ResultSet resultSet = statement.executeQuery(sql); //STEP 3: execute query, assign result to a ResultSet object

			if (resultSet.next()) //STEP 4: Extract data from result set
			{
				String brugernavn = resultSet.getString("username");
				System.out.println("Bruger " + brugernavn + " er logget på");
				resultSet.absolute(1);  // 1. set the row counter to any legal value
				resultSet.updateString("username", "Samuelsen");//  2. specify change
				resultSet.updateRow();
			} else {
				System.out.println("Ukendt bruger.");
			}

			resultSet.close(); //STEP 5: Clean-up environment
			statement.close();
		}catch(SQLException se){
			System.out.println("Error connecting to MySQL Server");
			se.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("Goodbye!");
	}



	public void insert(String username, String password){

		try
		{
			String insertTableSQL = "INSERT INTO users" + " VALUES" + "(null,?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);
			int random = (new Random()).nextInt(100);
			preparedStatement.setString(1, username + random);
			preparedStatement.setString(2, password);
			// execute insert SQL stetement
			int rowcount = preparedStatement.executeUpdate();
			System.out.println("Inserted: " + rowcount + " rows.");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void update(User user){
		int id = user.getId();
		String username = user.getUsername();
		String password = user.getPassword();
		try
		{
			String sql = "UPDATE users set username=?, password=? where idusers=?";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, username );
			preparedStatement.setString(2, password);
			preparedStatement.setInt(3, id);
			int rowcount = preparedStatement.executeUpdate();
			System.out.println("Updated: " + rowcount + " rows.");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void deleteUser(User user){
		int id = user.getId();
		try
		{
			String sql = "DELETE FROM users WHERE idusers=?";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			int rowcount = preparedStatement.executeUpdate();
			System.out.println("Query affected: " + rowcount + " rows.");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
