package com.example.sj8;

import com.example.sj8.database.MySQLdb;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
public class HomeController {

    List<User> users = new ArrayList<>();
    Map<Integer,User> map = new HashMap<>();
    private int counter = 0;
    private MySQLdb mySQLdb = new MySQLdb();  // this connection is meant to STAY alive for the entire lifetime of this application

    @RequestMapping(value = {"/","/inde"}, method = RequestMethod.GET)
    public String inde(Model model){
        users = mySQLdb.getAllUsers();
        model.addAttribute("users", users);
        return "inde";
    }



    @RequestMapping(value = { "/", "/inde"}, method = RequestMethod.POST, params = "create")
    public String create(@ModelAttribute User user, Model model){
        mySQLdb.insert(user.getUsername(), user.getPassword());
        users = mySQLdb.getAllUsers();
        model.addAttribute("users", users);
        System.out.println("received POST request " + user);
        return "inde";
    }



    @RequestMapping(value = { "/", "/inde"}, method = RequestMethod.POST, params = "update")
    public String updateForm(@ModelAttribute User user, Model model){

        mySQLdb.update(user);
        users = mySQLdb.getAllUsers();
        model.addAttribute("users", users);
        System.out.println("received POST request " + user);
        return "inde";
    }

    @RequestMapping(value = { "/", "/inde"}, method = RequestMethod.POST, params = "delete")
    public String delete(@ModelAttribute User user, Model model){
        mySQLdb.deleteUser(user);
        users = mySQLdb.getAllUsers();
        model.addAttribute("users", users);
        System.out.println("received POST request " + user);
        return "inde";
    }


    @RequestMapping(value = "/inde2")
    public String thymeleaf(@ModelAttribute User user, Model model)
    {
        // Params: HttpServletRequest request, HttpServletResponse response, ServletContext sCtx
//        WebContext ctx = new WebContext(request,response,sCtx); // doesn't work
        counter++;
        user.setId(counter);
        map.put(counter, user);
        //users.add(user);
        //model.addAttribute("users", users);
        model.addAttribute("map", map);
        return "inde2";
    }


    @RequestMapping(value = {"/inde2"}, method = RequestMethod.POST, params = "update")
    public String updateForm2(@ModelAttribute User user, Model model){
        map.put(user.getId(), user);
        model.addAttribute("map", map);
        //model.addAttribute("users", users);
        System.out.println("received POST request " + user);
        return "inde2";
    }

    @RequestMapping(value = { "/inde2"}, method = RequestMethod.POST, params = "delete")
    public String delete2(@ModelAttribute User user, Model model){
        //  users.remove(user.getId());
        System.out.println("map size: " + map.size());
        map.remove(user.getId());
        System.out.println("map size: " + map.size());
        model.addAttribute("map", map);
        //model.addAttribute("users", users);
        System.out.println("received POST request " + user);
        return "inde2";
    }
}
